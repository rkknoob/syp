@extends('layouts.endless2')

@section('content')
    <style>
        .panel-body{
            border-bottom: 1px solid #f1f5fc;
        }
    </style>

    <style>
        #map {
            width: 100%;
            height: 400px;
            background-color: grey;
        }
    </style>
    <div id="map"></div>
    <div id="vue-app">
       @{{message}}
    </div>
    










@endsection


@section('javascript')

    <script>


        function initMap() {
            // The location of Uluru
            var uluru = {lat: 13.8072562, lng: 100.5697148};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 4, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});
        }

    </script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_GoaYuQox3TD-hofh9AZ2hBct4Bq6dc8&callback=initMap">
    </script>


@endsection
