@extends('layouts.bbscard1')

@section('content')

    <div id="breadcrumb">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="/home"> Home</a></li>
            <li class="active">Clear Data</li>
        </ul>
    </div><!-- breadcrumb -->
    <br>

    <div class="container">
        <form class="form-horizontal form-border no-margin"  id="type-constraint">
            {{ csrf_field() }}

            <div class="panel panel-danger " >
                <div class="panel-heading">
                    <h4> <i class="fa fa-search " ></i>เพิ่ม Location</h4>
                </div>


                <div class="panel-body" hidden>
                    <div class="form-group">
                        <label for="depart_no" class="col-md-2 control-label">Location :</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="location" id="location" value="{{ old('id') }}">
                        </div>
                        <label for="depart_no" class="col-md-2 control-label">Zone : </label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="user_code" id="user_code" value="{{ old('id') }}" maxlength ="8">
                        </div>

                        <div class="col-md-2">
                            <button type="button" class="btn btn-success" onclick="save_data()">บันทึกข้อมูล</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="depart_no" class="col-md-2 control-label">Division :</label>
                        <div class="col-md-3">
                            <select name="division" id="division"  class="form-control">
                                <option value="1">Prime</option>
                                <option value="2">Reseach</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="container">
        <form class="form-horizontal form-border no-margin"  id="type-constraint">
            {{ csrf_field() }}

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4> <i class="fa fa-database"></i>Clear Data</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-success deletet" id="cleardata">ล้างข้อมูล รอบ 1</button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-success deletet" id="cleardata2">ล้างข้อมูล รอบ 2</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection


@section('javascript')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script>

        $(function(){
            $('table').dataTable({

                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                responsive: true


            });
        });



        function onDelete (id) {

            var result = confirm("Want to delete?");
            if (result) {
                //Logic to delete the item
                $.get('/user_login/delete/'+id,function (r) {

                    $('[data-tr='+id+']').remove();

                });
            }
        }



        function setNextFocus(ctrl,objId){
            if (ctrl.maxLength == ctrl.value.length){
                var obj=document.getElementById(objId);
                if (obj){
                    obj.focus();
                }
            }
        }


        $('#user_code').bind('keyup', function(e){

            var user_code=$('#user_code').val();

            if ($(this).val().length == 8){



            }

        });

        $(document).on("click","#cleardata",function(e){

            if(confirm("ต้องการลบใช่รึไหม")){

                $.ajax({
                    dataType: 'json',
                    type:'GET',
                    url: '/count_sheet/cleardata/clear',

                    success: function(datas){
                        swal({
                            type: datas.type,
                            title: datas.title
                        });
                    }

                })

            }else{

                return false;

            }

        });

        $(document).on("click","#cleardata2",function(e){

            if(confirm("ต้องการลบใช่รึไหม")){

                $.ajax({
                    dataType: 'json',
                    type:'GET',
                    url: '/count_sheet/cleardata/clear/round',

                    success: function(datas){
                        swal({
                            type: datas.type,
                            title: datas.title
                        });
                    }

                })

            }else{

                return false;

            }

        });






    </script>

@endsection
