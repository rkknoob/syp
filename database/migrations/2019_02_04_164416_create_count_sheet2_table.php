<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountSheet2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('count_sheet_2', function (Blueprint $table) {
            $table->increments('id');
            $table->string('zone');
            $table->string('status');
            $table->string('user_borrow');
            $table->string('user_receive');
            $table->string('status');
            $table->string('location');
            $table->string('type');
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('count_sheet_2');
    }
}
