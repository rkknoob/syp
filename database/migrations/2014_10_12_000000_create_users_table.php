<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::create('bbs_card', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_time');
            $table->string('user_login', 10)->nullable();
            $table->string('user_bbs', 10)->nullable();
            $table->string('safaty_1', 1)->nullable();
            $table->string('safaty_2', 1)->nullable();
            $table->string('safaty_3', 1)->nullable();
            $table->string('safaty_4', 1)->nullable();
            $table->string('safaty_5', 1)->nullable();
            $table->string('safaty_6', 1)->nullable();
            $table->string('safaty_7', 1)->nullable();
            $table->string('safaty_8', 1)->nullable();
            $table->string('flag_con', 1)->nullable();
            $table->text('drive')->nullable();
            $table->text('etc')->nullable();
            $table->string('date_time_created', 50)->nullable();
            $table->string('Status_safaty', 2)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('tb_mistake', function (Blueprint $table) {
            $table->string('mistake_code', 255)->nullable();
            $table->string('description_mistake', 255)->nullable();
            $table->increments('cnt_alert', 4)->nullable();
            $table->string('warning_flag', 1)->nullable();
            $table->timestamps('updated_at');
            $table->timestamps('created_at');
            $table->string('group', 255)->nullable();
            $table->string('group_department', 255)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });


        Schema::create('tb_transection', function (Blueprint $table) {
            $table->increments('No', 20);
            $table->string('user_code', 10)->nullable();
            $table->string('mistake_code', 255)->nullable();
            $table->string('type_warning', 255)->nullable();
            $table->increments('D_devolop', 11)->nullable();
            $table->string('warning_status', 2)->nullable();
            $table->text('description')->nullable();
            $table->string('path_picture', 255)->nullable();
            $table->string('path_picture_Serv', 255)->nullable();
            $table->string('user_login', 255)->nullable();
            $table->dateTime('user_date')->nullable();
            $table->string('flag_print', 1)->nullable();
            $table->timestamps('updated_at');
            $table->timestamps('created_at');
            $table->text('signatures')->nullable();
            $table->string('flag_comfirm', 1)->nullable();
            $table->text('devolop_plan')->nullable();
            $table->string('timlength')->nullable();
            $table->string('mistake_type')->nullable();
            $table->dateTime('date_review')->nullable();
            $table->text('suggestion')->nullable();
            $table->string('id_temp', 10)->nullable();
            $table->rememberToken();
            $table->timestamps();

        });







    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bbs_card');
    }
}
