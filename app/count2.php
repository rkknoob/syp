<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class count2 extends Model
{
    protected $table = 'count_sheet2';

    protected $fillable = [
        'id','user_borrow','location','user_receive','type','time_start','status','zone'
    ];
}
