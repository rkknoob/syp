<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;

class PreventBackHistory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (str_contains($request->url(), 'home')) {
            return $response->header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate')
                ->header('Pragma', 'no-cache')
                ->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
        }
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }

        if($request->url()){


        }


        return $response;
    }


}
