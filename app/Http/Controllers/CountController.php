<?php

namespace App\Http\Controllers;

use App\Count1;
use App\count2;
use Illuminate\Http\Request;
use DB;
use Input;
use Carbon\Carbon;
use App\count_sheet;
use Session;
use Illuminate\Support\Str;

class CountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
{


    return view('count.index');
}

    public function index2()
    {


        return view('count.index2');
    }


    public function reci()
    {
        return view('count.reci');
    }

    public function reci2()
    {
        return view('count.reci2');
    }



    public function insert(Request $request)
    {
    $input= $request->all();

    $trip = $input['location'];


    $loc = Input::get('location');
    $usere = Input::get('user_code');
    $division = Input::get('division');


        $locationstatus = DB::table('count_sheet')
            ->where('location' ,'=', $loc)
            ->value('status');


    //    $test =  substring($t2,length($t2)-2 , 2);

        if($loc) {
            $st_dev = DB::table('count_sheet')
                ->where('location' ,'=', $loc)
                ->value('location');
            if($st_dev){
               if(($locationstatus == 'S') || ($locationstatus == 'E')){

                   return 98;
               }
                $user = DB::table('count_sheet')->where('location', $loc)->where('type', $division)->first();
                if($user){

                    $cutzone  = Str::substr($loc, -2);
                    DB::statement("UPDATE count_sheet
SET status= 'S',user_borrow = '$usere',time_start = NOW()
WHERE zone = '$cutzone' and type = '$division' ");

                    return 1;
                }else{

                    return 97;
                }

            }else {

                return 99;
            }

        }
        return response()->json([
            'datas' => $trip,
        ]);





}


    public function insert2(Request $request)
    {

        $input= $request->all();

        $trip = $input['location'];

        $t2 = Input::get('location');
        $t3 = Input::get('user_code');

        $st_dev = DB::table('count_sheet')
            ->where('location' ,'=', $t2)
            ->value('status');


        if($st_dev != 'S') {

        return 2;
        }else {

            DB::statement("UPDATE count_sheet SET status= 'E',user_receive = '$t3',time_end = NOW()
WHERE location = '$t2'");

            return 1;
        }
    }


    public function insert3(Request $request)
    {


        $location = $request->location;
        $checklocation = Count1::where('location',$location)->count();
        $checklocationSheet2 = Count2::where('location',$location)->count();

        $test = Count2::where('location',$location)->get();

        $checklocationtype = Count1::where('location',$location)->value('type');
        $checklocationzone = Count1::where('location',$location)->value('zone');
        $request->request->add(['type' => $checklocationtype]);
        $request->request->add(['zone' => $checklocationzone]);
        $request->request->add(['status' => 'S']);
        $carbon = Carbon::now();
        $request->request->add(['time_start' => $carbon]);
        $input = $request->all();




        if($checklocation){
            if($checklocationSheet2 == 0){
                $item = count2::create($input);
                return 1;
            }else {
                return 98;
            }
        }

      //  $email = DB::table('users')->where('name', 'John')->value('email');

        return 99;

    }


    public function insert4(Request $request)
    {

        $input= $request->all();

        $t2 = Input::get('location');
        $t3 = Input::get('user_receive');


        $st_dev = DB::table('count_sheet2')
            ->where('location' ,'=', $t2)
            ->value('status');


        if($st_dev != 'S') {

            return 98;
        }else {

            DB::statement("UPDATE count_sheet2 SET status= 'E',user_receive = '$t3',time_end = NOW()
WHERE location = '$t2'");

            return 1;
        }







    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function data()
    {
        return view('count.data');
    }

    public function data_round2(Request $request)
    {


        $input= $request->all();

        $t2 = Input::get('location');

        $check_id  = DB::table('count_sheet2')->where('location', '=', $t2)->count();

        if($check_id == 0){

            DB::insert('insert into count_sheet2 (location,round,status) values (?, ?, ?)', [$t2,'2','N']);
            return 1;
        }else {


            return 2;
        }



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
