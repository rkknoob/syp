<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClearLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('count.cleardata');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cleardata()
    {
        DB::statement("UPDATE count_sheet
SET status= null,user_borrow = null,time_start = null,user_receive = null,time_end = null");

        return response()->json([
            'type' => 'success',
            'title' => 'ล้างสำเร็จ'
        ]);
    }

    public function round()
    {
        DB::statement("TRUNCATE TABLE count_sheet2;");

        return response()->json([
            'type' => 'success',
            'title' => 'ล้างสำเร็จ'
        ]);
    }
}
