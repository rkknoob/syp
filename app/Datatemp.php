<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datatemp extends Model
{

    protected $table = 'datatemp';
    public static $key = 'id';

    protected $fillable = [
        'id','document_date1','user_code','date_work','detail'
    ];


    public function mistake () {

        return $this->belongsTo(tb_mistake::class);

    }

    public function test () {

        $datatemp = DB::select( DB::raw("select a.id,DATE_FORMAT(a.date_work,'%Y-%m-%d') as date_work,a.user_code,a.detail,a.mistake_code,b.description_mistake,flag_con,upload_pdf from datatemp a
LEFT JOIN tb_mistake b on a.mistake_code = b.mistake_code
where flag_con = 'N' ORDER BY a.date_work desc 
")

        );
        return $datatemp;

    }

    public function createLog() {


    }



}
