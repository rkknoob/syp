<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Count1 extends Model
{
    protected $table = 'count_sheet';

    protected $fillable = [
        'id','user_borrow','location','user_receive'
    ];
}
